// 1) Promise - обещание предоставить результат позже. Промисы позваляют обрабатывать отложенные во времени события.
// Промис может вернуть результат или может вернуть ошибку, если результат представить невозно.
// Состаяния промиса : pending(ожидание), fulfilled (исполнен), rejected (отклонен)

const myPromise = new Promise( resolve => {
    setTimeout(() => resolve(5), 2000)
})
myPromise.then(value => {
    console.log(value)
    return new Promise (resolve => {
        setTimeout(() => resolve(value * 5), 3000)
    })
}).then(value => {
    console.log(value)
    return new Promise(resolve => {
        setTimeout(() => resolve(value + 5), 2000)
    })
}).then(value => console.log(value)); // 5 20 30


let a = new Promise((resolve, reject) => {
  // то же что reject(new Error("o_O"))
  throw new Error("o_O");
})

a.catch(alert); // Error: o_O

// 2) Конструкция async/await - специальный синтаксис для упращения работы с промисами.Появился в es6.
//Асинхронная ф-ия -функция , которая вместо значения возвращает промис.
const asyncFun = async () => {
    return 'Success!'
}
asyncFun().then(value => console.log(value)) // Success!
//await синтаксис возможен только внутри асинхронной(async) функции.
//async ф-ия всегда возвращает Promise. async ф-ия ожидает результата инструкции await и не выполняет  последующие инструкции

async function f() {

  let promise = new Promise((resolve, reject) => {
    setTimeout(() => resolve("готово!"), 5000)
  });

  let result = await promise; // будет ждать, пока промис не выполнится (*)

  alert(result); // "готово!"
}

f();

// 3) Контекст JS - концепция окружения , в котором выполняется код JS. Код всегда выполняется в контексте.
// Контекстом еще часто называют значение переменной this внутри функции. This - ссылка на объект,метод которого мы вызываем.

const user = {
    name: 'Аяна',
    sayHello() {
        console.log(`Привет, я - ${this.name}`)
    }
}
user.sayHello()

// 4) Замыкание JS - это комбинация ф-ции и лексического окружения, в котором эта ф-ия была орпеделена.
// Заыкание дает доступ к внешней ф-ции из внутренней ф-ции.

function person() {
  let name = 'John';

  return function displayName() {
    console.log(name);
  };
}
let peter = person();
peter(); //  'John'

// 5) Стрелочные ф-ии - функции ,в которых нет ключевого слова function. Имеет специальный синтекс.
//Стрелочная ф-ция - это выражения. Стрелочные ф-ции всегда анонимные. Появилась в es6. Отличается контекстом.
const myFunction = (a, b) => {
    let c
    a = a + 1
    c = a + b
    return c
}
myFunction(5, 3); // 9